using NUnit.Framework;

namespace Fibonacci
{
    public class FibonacciShould
    {
        [TestCase(0,0)]
        [TestCase(1,1)]
        [TestCase(2,1)]
        [TestCase(3,2)]
        [TestCase(4,3)]
        [TestCase(5,5)]
        [TestCase(6,8)]
        [TestCase(7,13)]
        [TestCase(8,21)]
        public void be_fibonacci_for_position(int position, int expectedOuput)
        {
            Assert.AreEqual(expectedOuput, Fibonacci.ForPosition(position));
        }
    }
}