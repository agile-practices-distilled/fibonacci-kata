﻿using System;

namespace Fibonacci
{
    public static class Fibonacci
    {
        public static int ForPosition(int position)
        {
            if (position > 1)
                return ForPosition(position - 1) + ForPosition(position - 2);

            return position;
        }
    }
}